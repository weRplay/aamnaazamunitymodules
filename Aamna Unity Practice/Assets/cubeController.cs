﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cubeController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("right")) {
            transform.Translate(0.05f, 0, 0);
        }
        else if (Input.GetKey("left")) {
            transform.Translate(-0.05f, 0, 0);
        }
        else if (Input.GetKey("up")) {
            transform.Translate(0, 0f, 0.05f);
        }
        else if (Input.GetKey("down")) {
            transform.Translate(0, 0f, -0.05f);
        }



    }
}
